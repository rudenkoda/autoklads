from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator
import pdfminer
from scipy.ndimage import binary_fill_holes

from collections import Counter
import numpy as np

import matplotlib.pyplot as plt

from skimage.color import rgb2gray
import skimage.morphology as m
from skimage.transform import downscale_local_mean, rescale, resize

import json
import cv2
# from yolo.backend.utils.box import draw_scaled_boxes
import os
# import yolo

import cv2
from skimage import morphology as m
import numpy as np

import os
import matplotlib.pyplot as plt

from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
from pdf2image import convert_from_path, convert_from_bytes

# Open a PDF file.
types = set()


def parse_file(f_path):
    fp = open(f_path, 'rb')

    # Create a PDF parser object associated with the file object.
    parser = PDFParser(fp)

    # Create a PDF document object that stores the document structure.
    # Password for initialization as 2nd parameter
    document = PDFDocument(parser)

    # Check if the document allows text extraction. If not, abort.
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed

    # Create a PDF resource manager object that stores shared resources.
    rsrcmgr = PDFResourceManager()

    # Create a PDF device object.
    device = PDFDevice(rsrcmgr)

    # BEGIN LAYOUT ANALYSIS
    # Set parameters for analysis.
    laparams = LAParams()

    # Create a PDF page aggregator object.
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)

    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    import re
    clads = []
    texts = []
    lift_names = []
    lifts = []

    def parse_obj(lt_objs):
        # loop over the object list
        for obj in lt_objs:

            # if it's a textbox, print text and location
            if isinstance(obj, pdfminer.layout.LTTextBoxHorizontal):
                # print("%6d, %6d, %s" % (obj.bbox[0], obj.bbox[1], obj.get_text().replace('\n', '_')))
                text = obj.get_text().replace('\n', '_')
                #                 print(text)
                if ("Лифт" in text):
                    lift_names.append(text)
                    lifts.append((obj.bbox[0], obj.bbox[1], obj.bbox[2], obj.bbox[3]))
                if re.match("\d+_", text):
                    text = text.split("_")[0]
                    texts.append(text)
                    clads.append((obj.bbox[0], obj.bbox[1], obj.bbox[2], obj.bbox[3]))

            # if it's a container, recurse
            elif isinstance(obj, pdfminer.layout.LTFigure):
                parse_obj(obj._objs)
            else:
                if type(obj) not in types:
                    types.add(type(obj))
                    print(type(obj))

    # loop over all pages in the document
    for page in PDFPage.create_pages(document):
        # read the page into a layout object
        interpreter.process_page(page)
        layout = device.get_result()

        # extract text from this object
        parse_obj(layout._objs)
    return texts, clads, layout, lift_names, lifts


def get_main_area(img):
    if np.max(img) < 10:
        img = img * 255
    main_area = np.logical_and(image_[:, :, 0] * 255 > 200, image_[:, :, 2] * 255 < 200)
    main_area = m.remove_small_objects(main_area, 1)
    main_area = m.remove_small_holes(main_area, 1)
    return m.convex_hull_image(m.binary_erosion(main_area, selem=m.selem.square(1)))


def get_walk_ways(img):
    if np.max(img) < 10:
        img = img * 255
    walk_ways = np.logical_and(img[:, :, 2] > 200, get_main_area(img))
    walk_ways = m.binary_erosion(img[:, :, 2] < 200, selem=m.selem.disk(1))
    walk_ways = m.remove_small_objects(walk_ways, 4000)

    walk_ways = np.logical_not(walk_ways)
    walk_ways = np.logical_and(walk_ways, get_main_area(img))
    return m.remove_small_objects(walk_ways, 10000)

    walk_ways = np.logical_and(walk_ways, get_main_area(img))
    walk_ways = m.remove_small_objects(walk_ways, 10000)
    return walk_ways  # m.binary_opening(walk_ways, selem=m.selem.disk(5))


def get_walls(img):
    vertical = m.binary_opening(img[:, :, 0] < 0.5, selem=m.selem.rectangle(3, 1))
    horisontal = m.binary_opening(img[:, :, 0] < 0.5, selem=m.selem.rectangle(1, 3))
    return m.binary_dilation(np.logical_and(vertical, horisontal), selem=m.selem.disk(2))


def find_klads(img):
    walls = get_walls(img)
    area = ~get_main_area(img)
    if np.max(img) < 10:
        img = img * 255
    res = img[:, :, 0] > 200
    res = np.logical_and(img[:, :, 1] > 200, res)
    res = m.binary_dilation(res, selem=m.selem.rectangle(2, 2))
    res = m.binary_dilation(res, selem=m.selem.rectangle(3, 3))
    res = m.remove_small_holes(res, 100)
    res = m.remove_small_objects(res)
    res = m.binary_erosion(res, selem=m.selem.disk(5))
    res = m.label(res)
    s = np.zeros_like(res)
    for each in np.unique(res):
        if each != 0:
            s += m.convex_hull_image((res == each)).astype(int)
    s[walls] = np.min(s)
    s[area] = np.min(s)

    return m.label(s)

def bbox2(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    return rmin, rmax, cmin, cmax


def get_skeleton(img):
    s = binary_fill_holes(find_klads(img) > 1).astype(int)
    w = get_walk_ways(img).astype(int)
    z = m.binary_closing((w + s),
                         selem=m.selem.square(5))
    wallz = m.remove_small_objects(get_walls(img), 300)

    z[wallz] = False

    z = m.skeletonize(z)

    z[wallz] = False
    span = m.remove_small_objects(np.logical_and(z > 0, s == 0), connectivity=2).astype(int)
    return m.skeletonize(span + s)


def get_biggest_connected_thing(mask):
    freq_label = Counter(m.label(mask).flatten()).most_common()[1][0]
    # print(freq_label)
    mask[m.label(mask) != freq_label] = 0
    return mask


def get_lift(img):
    base = img[:, :, 2] * 255 < 200
    font = m.remove_small_holes(np.logical_or(m.binary_opening(base, selem=m.selem.rectangle(6, 1)),
                                              m.binary_opening(base, selem=m.selem.rectangle(1, 6))))
    base[font] = 0
    base = np.logical_and(m.binary_closing(base, selem=m.selem.disk(5)), get_walk_ways(img))
    base = get_biggest_connected_thing(base)
    base = m.convex_hull_image(base)
    return base


def plot_walk_path(n, mapping_dict, graph, lift_nodes, save_path=None, figsize=10):
    target = mapping_dict[n]

    plt.figure(figsize=(figsize, figsize))
    plt.title(n)
    min_path = np.inf
    pss = None
    for lift_node in lift_nodes:
        path = set(list(nx.all_shortest_paths(graph, lift_node, target))[0])

        plt.imshow(img, cmap="gray")
        path_len = 0
        plot_path = []
        for (s, e) in graph.edges():
            if s in path and e in path:
                ps = graph[s][e]['pts']
                #                 plt.plot(ps[:,1], ps[:,0], 'blue')
                plot_path.append((ps[:, 1], ps[:, 0]))
                path_len += np.sqrt(
                    np.square(np.int64(ps[:, 1][0] - ps[:, 1][-1])) + np.square(np.int64(ps[:, 0][0] - ps[:, 0][-1])))
        node, nodes = graph.node, graph.nodes()
        if path_len < min_path:
            min_path = path_len
            pss = np.array([node[i]['o'] for i in nodes if i in path])
            best_path = plot_path
    if pss is not None:
        plt.plot(pss[:, 1], pss[:, 0], 'r.')
        for edge in best_path:
            plt.plot(edge[0], edge[1], 'blue')
    if save_path is not None:
        plt.savefig(save_path)